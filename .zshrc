export ZSH="/home/yuan/.oh-my-zsh"

ZSH_THEME="bira"

plugins=(
  git
  zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh
source /home/yuan/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZSH_CUSTOM/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh


#编辑器
export EDITOR=vim
#历史纪录条目数量
export HISTSIZE=10000
#注销后保存的历史纪录条目数量
export SAVEHIST=10000

export WORKON_HOME=$HOME/virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export PATH="/home/yuan/miniconda3/bin:$PATH"  
. /home/yuan/miniconda3/etc/profile.d/conda.sh
